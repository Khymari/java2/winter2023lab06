import java.util.Random;

public class Die
{
	private int faceValue;
	private Random rand;
	
	//Constructor
	public Die()
	{
		this.faceValue = 1;
		this.rand = new Random();
	}
	
	//Getters
	public int getFaceValue()
	{
		return this.faceValue;
	}
	
	//Normal methods
	public void roll()
	{
		this.faceValue = rand.nextInt(6) + 1;
	}
	
	//To String
	public String toString()
	{
		return "Face value is: " + this.faceValue;
	}
}