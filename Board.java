public class Board
{
	private Die dieA;
	private Die dieB;
	private boolean[] hasValueBeenRolled;
	
	//Constructor
	public Board()
	{
		this.dieA = new Die();
		this.dieB = new Die();
		this.hasValueBeenRolled = new boolean[12];
	}
	
	//Normal methods
	public boolean playATurn()
	{
		dieA.roll();
		dieB.roll();
		
		System.out.println(dieA);
		System.out.println(dieB);
		
		int sumofDice = dieA.getFaceValue() + dieB.getFaceValue();
		
		if(!hasValueBeenRolled[(sumofDice - 1)])
		{
			hasValueBeenRolled[(sumofDice - 1)] = true;
			System.out.println("Closing tile equal to sum: " + sumofDice);
			return false;
		}
		else if(!hasValueBeenRolled[(dieA.getFaceValue() - 1)])
		{
			hasValueBeenRolled[(dieA.getFaceValue() - 1)] = true;
			System.out.println("Closing tile with the same value as die one: " + dieA.getFaceValue());
			return false;
		}
		else if(!hasValueBeenRolled[(dieB.getFaceValue() - 1)])
		{
			hasValueBeenRolled[(dieB.getFaceValue() - 1)] = true;
			System.out.println("Closing tile with the same value as die two: " + dieB.getFaceValue());
			return false;
		}
		else
		{
			System.out.println("All the tiles for these values are already shut.");
			return true;
		}
	}
	
	//To String
	public String toString()
	{
		String board = "";
		for(int i = 0; i < this.hasValueBeenRolled.length - 1; i++)
		{
			if(hasValueBeenRolled[i])
				board += (i + 1) + ", ";
			else
				board += "X, ";
		}
		if(this.hasValueBeenRolled[11])
			board += (11 + 1);
		else
			board += "X";
		return board;
	}
}	