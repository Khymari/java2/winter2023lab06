import java.util.Scanner;

public class Jackpot
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("What is your name?");
		String name = scan.next();
		System.out.println("Hello " + name + ", welcome to The Jackpot!");
		
		Board board = new Board();
		
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(!gameOver)
		{
			System.out.println(board);
			if(board.playATurn())
				gameOver = true;
			else
				numOfTilesClosed++;
		}
		if(numOfTilesClosed >= 7)
			System.out.println("Congratulations! " + name + ", you have won the jackpot!");
		else
			System.out.println(name + " you have lost the jackpot.");
	}
}